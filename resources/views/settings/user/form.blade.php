{!! Form::hidden('user_id', Auth::user()->id  ) !!}
<div class="form-group {{ $errors->has('region_id') ? 'has-error' : ''}}">
    {!! FORM::label('region_id', 'Region Name *', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! FORM::select('region_id', $region, !empty($user->branch->zone->region->id)? $user->branch->zone->region->id : null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Select Region']) !!}
        {!! $errors->first('region_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('zone_id') ? 'has-error' : ''}}">
    {!! FORM::label('zone_id', 'Zone Name *', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! FORM::select('zone_id', $zone, !empty($user->branch->zone->id)? $user->branch->zone->id : null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Select Zone']) !!}
        {!! $errors->first('zone_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('branch_id') ? 'has-error' : ''}}">
    {!! FORM::label('branch_id', 'Branch Name *', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! FORM::select('branch_id', $branch, null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Select Branch']) !!}
        {!! $errors->first('branch_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    <div class="col-sm-6 col-sm-offset-3">
        <hr/>
    </div>
</div>
<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! FORM::label('name', 'Full Name *', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! FORM::text('name', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Enter user name']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    {!! FORM::label('email', 'Email *', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! FORM::text('email', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Enter email ID']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@if(!isset($user))
<div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
    {!! FORM::label('password', 'Password *', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! FORM::password('password', ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Enter password']) !!}
        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('retype_password') ? 'has-error' : ''}}">
    {!! FORM::label('retype_password', 'Retype Password *', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! FORM::password('retype_password', ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Enter password']) !!}
        {!! $errors->first('retype_password', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@endif
<div class="form-group {{ $errors->has('contact') ? 'has-error' : ''}}">
    {!! FORM::label('contact', 'Contact No', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! FORM::text('contact', null, ['class' => 'form-control', 'placeholder' => 'Enter contact no']) !!}
        {!! $errors->first('contact', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
    {!! FORM::label('address', 'Address', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::textarea('address', null,['class'=>'form-control', 'rows' => 3, 'cols' => 40, 'placeholder' => 'Enter address']) !!}
        {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('role') ? 'has-error' : ''}}">
    {!! FORM::label('role', 'User Type *', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! FORM::select('role', ['user' => 'User', 'admin' => 'Admin'], null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Select Type']) !!}
        {!! $errors->first('role', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    <div class="col-sm-offset-3 col-sm-6">
        {!! FORM::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary pull-right']) !!}
    </div>
</div>

