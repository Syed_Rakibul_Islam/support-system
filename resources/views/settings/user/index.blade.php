@extends('layouts.app')

@section('content')
    <div class="col-md-12 ">
        <div  class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    <i class="fa fa-table"></i>
                    User
                    <a href="{{ url('/settings/user/create') }}" class="btn btn-primary btn-sm" title="Add New User">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                    </a>
                    <div class="bars pull-right">
                        <a href="#"><i class="maximum fa fa-expand" data-toggle="tooltip" data-placement="bottom" title="Maximize"></i></a>
                        <a href="#"><i class="minimize fa fa-chevron-down" data-toggle="tooltip" data-placement="bottom" title="Collapse"></i></a>
                    </div>
                </div>
            </div>
            <div class="panel-body">

                <table class="table table-striped table-bordered width-100 cellspace-0" >
                    <thead>
                        <tr>
                            <th>User Name</th>
                            <th>Role</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($user as $item)
                        <tr>
                            <td>{{ $item->name }}</td>
                            @if($item->role == 'user')
                                <td class="bg-info">User</td>
                            @elseif($item->role == 'admin')
                                <td class="bg-warning">Admin</td>
                            @else
                                <td class="bg-info"> User</td>
                            @endif
                            @if($item->status == 'Suspend')
                                <td class="bg-danger">Suspend</td>
                            @else
                                <td class="bg-success">Active</td>
                            @endif
                            <td>
                            <a href="{{ url('/settings/user/' . $item->id) }}" title="View User"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></button></a>

                            @if(!empty(Auth::user()->super->id))
                                <a href="{{ url('/settings/user/' . $item->id . '/edit') }}" title="Edit User"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                @if($item->status == 'Suspend')
                                    {!! FORM::open([
                                    'method'=>'POST',
                                    'url' => ['/settings/user/active', $item->id],
                                    'style' => 'display:inline'
                                    ]) !!}
                                    {!! FORM::button('<i class="fa fa-check" aria-hidden="true"></i>', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-success btn-xs',
                                    'title' => 'Active User',
                                    'onclick'=>'return confirm("Confirm active?")'
                                    )) !!}
                                    {!! FORM::close() !!}
                                @else
                                    {!! FORM::open([
                                    'method'=>'DELETE',
                                    'url' => ['/settings/user', $item->id],
                                    'style' => 'display:inline'
                                    ]) !!}
                                    {!! FORM::button('<i class="fa fa-ban" aria-hidden="true"></i>', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Suspend User',
                                    'onclick'=>'return confirm("Confirm suspend?")'
                                    )) !!}
                                    {!! FORM::close() !!}
                                @endif
                            @elseif($item->role != 'admin')
                                <a href="{{ url('/settings/user/' . $item->id . '/edit') }}" title="Edit User"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                @if($item->status == 'Suspend')
                                    {!! FORM::open([
                                    'method'=>'POST',
                                    'url' => ['/settings/user/active', $item->id],
                                    'style' => 'display:inline'
                                    ]) !!}
                                    {!! FORM::button('<i class="fa fa-check" aria-hidden="true"></i>', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-success btn-xs',
                                    'title' => 'Active User',
                                    'onclick'=>'return confirm("Confirm active?")'
                                    )) !!}
                                    {!! FORM::close() !!}
                                @else
                                    {!! FORM::open([
                                    'method'=>'DELETE',
                                    'url' => ['/settings/user', $item->id],
                                    'style' => 'display:inline'
                                    ]) !!}
                                    {!! FORM::button('<i class="fa fa-ban" aria-hidden="true"></i>', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Suspend User',
                                    'onclick'=>'return confirm("Confirm suspend?")'
                                    )) !!}
                                    {!! FORM::close() !!}
                                @endif
                            @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="pagination-wrapper"> {!! $user->render() !!} </div>
            </div>
        </div><!-- end panel -->
    </div><!-- end .col-md-12 -->

@endsection
