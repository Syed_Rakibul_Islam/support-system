﻿/**
 * Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

/* exported initSample */

if ( CKEDITOR.env.ie && CKEDITOR.env.version < 9 )
	CKEDITOR.tools.enableHtml5Elements( document );

// The trick to keep the editor in the sample quite small
// unless user specified own height.
CKEDITOR.config.height = 150;
CKEDITOR.config.width = 'auto';

var initSample = ( function() {
	var wysiwygareaAvailable = isWysiwygareaAvailable(),
		isBBCodeBuiltIn = !!CKEDITOR.plugins.get( 'bbcode' );

	return function() {
		var misProblemEditorElement = CKEDITOR.document.getById( 'mis_problem_details' );
		if(misProblemEditorElement){
			// :(((
			// Depending on the wysiwygare plugin availability initialize classic or inline editor.
			if ( wysiwygareaAvailable ) {
				CKEDITOR.replace( 'mis_problem_details' );
			} else {
				misProblemEditorElement.setAttribute( 'contenteditable', 'true' );
				CKEDITOR.inline( 'mis_problem_details' );

				// TODO we can consider displaying some info box that
				// without wysiwygarea the classic editor may not work.
			}
		}

		var misSolutionEditorElement = CKEDITOR.document.getById( 'mis_right_details' );
		if(misSolutionEditorElement){
		

			// Depending on the wysiwygare plugin availability initialize classic or inline editor.
			if ( wysiwygareaAvailable ) {
				CKEDITOR.replace( 'mis_right_details' );
			} else {
				misSolutionEditorElement.setAttribute( 'contenteditable', 'true' );
				CKEDITOR.inline( 'mis_right_details' );

				// TODO we can consider displaying some info box that
				// without wysiwygarea the classic editor may not work.
			}
		}

		var aisSolutionEditorElement = CKEDITOR.document.getById( 'ais_right_details' );
		if(aisSolutionEditorElement){
		

			// Depending on the wysiwygare plugin availability initialize classic or inline editor.
			if ( wysiwygareaAvailable ) {
				CKEDITOR.replace( 'ais_right_details' );
			} else {
				aisSolutionEditorElement.setAttribute( 'contenteditable', 'true' );
				CKEDITOR.inline( 'ais_right_details' );

				// TODO we can consider displaying some info box that
				// without wysiwygarea the classic editor may not work.
			}
		}

		var regularActivitiesEditorElement = CKEDITOR.document.getById( 'regular_activity_details' );
		if(regularActivitiesEditorElement){
		

			// Depending on the wysiwygare plugin availability initialize classic or inline editor.
			if ( wysiwygareaAvailable ) {
				CKEDITOR.replace( 'regular_activity_details' );
			} else {
				regularActivitiesEditorElement.setAttribute( 'contenteditable', 'true' );
				CKEDITOR.inline( 'regular_activity_details' );

				// TODO we can consider displaying some info box that
				// without wysiwygarea the classic editor may not work.
			}
		}

		var othersProblemEditorElement = CKEDITOR.document.getById( 'others_problem_details' );
		if(othersProblemEditorElement){
		

			// Depending on the wysiwygare plugin availability initialize classic or inline editor.
			if ( wysiwygareaAvailable ) {
				CKEDITOR.replace( 'others_problem_details' );
			} else {
				othersProblemEditorElement.setAttribute( 'contenteditable', 'true' );
				CKEDITOR.inline( 'others_problem_details' );

				// TODO we can consider displaying some info box that
				// without wysiwygarea the classic editor may not work.
			}
		}
		
		var othersSolutionEditorElement = CKEDITOR.document.getById( 'others_right_details' );
		if(othersSolutionEditorElement){
		

			// Depending on the wysiwygare plugin availability initialize classic or inline editor.
			if ( wysiwygareaAvailable ) {
				CKEDITOR.replace( 'others_right_details' );
			} else {
				othersSolutionEditorElement.setAttribute( 'contenteditable', 'true' );
				CKEDITOR.inline( 'others_right_details' );

				// TODO we can consider displaying some info box that
				// without wysiwygarea the classic editor may not work.
			}
		}
	};

	function isWysiwygareaAvailable() {
		// If in development mode, then the wysiwygarea must be available.
		// Split REV into two strings so builder does not replace it :D.
		if ( CKEDITOR.revision == ( '%RE' + 'V%' ) ) {
			return true;
		}

		return !!CKEDITOR.plugins.get( 'wysiwygarea' );
	}
} )();

