<?php

namespace App\Http\Controllers\Settings;

use App\Branch;
use App\Http\Controllers\Controller;
use App\Region;
use App\User;
use App\Zone;
use Illuminate\Support\Facades\Auth;
use Session;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        $user = User::whereNotIn('id', ['1'])->paginate($perPage);
        return view('settings.user.index', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $region = Region::pluck('name', 'id');
        $zone = Zone::pluck('name', 'id');
        $branch = Branch::pluck('name', 'id');
        return view('settings.user.create', compact('region', 'zone', 'branch'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|unique:users|max:255',
            'password' => 'required|max:255|same:retype_password',
            'contact' => 'max:255',
            'address' => 'max:255',
            'role' => 'required',
            'user_id' => 'required',
            'branch_id' => 'required'
        ]);
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'contact' => $request->contact,
            'address' => $request->address,
            'role' => $request->role,
            'status' => 'Active',
            'branch_id' => $request->branch_id,
            'user_id' => $request->user_id,
            'remember_token' => bcrypt(str_random(10)),
        ]);

        Session::flash('flash_success_msg', 'User added!');

        return redirect('settings/user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        if($id != 1){
            $user = User::findOrFail($id);
            return view('settings.user.show', compact('user'));
        }
        else{
            Session::flash('flash_warning_msg', 'This action could not perform!');
        }
        return redirect('settings/user');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        if($id != 1){
            $user = User::findOrFail($id);
            if(!empty(Auth::user()->super->id)){
                $region = Region::pluck('name', 'id');
                $zone = Zone::pluck('name', 'id');
                $branch = Branch::pluck('name', 'id');
                return view('settings.user.edit', compact('user', 'region', 'zone', 'branch'));
            }
            elseif(Auth::user()->role == 'admin' && $user->role != 'admin'){
                $region = Region::pluck('name', 'id');
                $zone = Zone::pluck('name', 'id');
                $branch = Branch::pluck('name', 'id');
                return view('settings.user.edit', compact('user', 'region', 'zone', 'branch'));
            }
            else{
                Session::flash('flash_warning_msg', 'This action could not perform!');
            }
        }
        else{
            Session::flash('flash_warning_msg', 'This action could not perform!');
        }
        return redirect('settings/user');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        if($id != 1){
            $user = User::findOrFail($id);
            if(!empty(Auth::user()->super->id)){
                $this->validate($request, [
                    'name' => 'required|max:255',
                    'email' => 'required|max:255',
                    'contact' => 'max:255',
                    'address' => 'max:255',
                    'role' => 'required',
                    'user_id' => 'required',
                    'branch_id' => 'required'
                ]);
                $user->update([
                    'name' => $request->name,
                    'email' => $request->email,
                    'contact' => $request->contact,
                    'address' => $request->address,
                    'role' => $request->role,
                    'status' => 'Active',
                    'branch_id' => $request->branch_id,
                    'user_id' => $request->user_id,
                ]);
                Session::flash('flash_success_msg', 'User updated!');
            }
            elseif(Auth::user()->role == 'admin' && $user->role != 'admin'){
                $this->validate($request, [
                    'name' => 'required|max:255',
                    'email' => 'required|max:255',
                    'contact' => 'max:255',
                    'address' => 'max:255',
                    'role' => 'required',
                    'user_id' => 'required',
                    'branch_id' => 'required'
                ]);
                $user->update([
                    'name' => $request->name,
                    'email' => $request->email,
                    'contact' => $request->contact,
                    'address' => $request->address,
                    'role' => $request->role,
                    'status' => 'Active',
                    'branch_id' => $request->branch_id,
                    'user_id' => $request->user_id,
                ]);
                Session::flash('flash_success_msg', 'User updated!');
            }
            else{
                Session::flash('flash_warning_msg', 'This action could not perform!');
            }
        }
        else{
            Session::flash('flash_warning_msg', 'This action could not perform!');
        }
        return redirect('settings/user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        if($id != 1){
            $user = User::findOrFail($id);

            if(!empty(Auth::user()->super->id))
            {
                $user->update([
                    'status' => 'Suspend',
                ]);
                Session::flash('flash_success_msg', 'User suspended!');
            }
            elseif(Auth::user()->role == 'admin' && $user->role != 'admin'){
                $user->update([
                    'status' => 'Suspend',
                ]);
                Session::flash('flash_success_msg', 'User suspended!');
            }
            else{
                Session::flash('flash_warning_msg', 'This action could not perform!');
            }
        }
        else{
            Session::flash('flash_warning_msg', 'This action could not perform!');
        }
        return redirect('settings/user');
    }
    public function active($id)
    {
        if($id != 1){
            $user = User::findOrFail($id);

            if(!empty(Auth::user()->super->id))
            {
                $user->update([
                    'status' => 'Active',
                ]);
                Session::flash('flash_success_msg', 'User activate!');
            }
            elseif(Auth::user()->role == 'admin' && $user->role != 'admin'){
                $user->update([
                    'status' => 'Active',
                ]);
                Session::flash('flash_success_msg', 'User activate!');
            }
            else{
                Session::flash('flash_warning_msg', 'This action could not perform!');
            }
        }
        else{
            Session::flash('flash_warning_msg', 'This action could not perform!');
        }
        return redirect('settings/user');
    }
}
