<?php

namespace App\Http\Controllers;

use App\Region;
use Illuminate\Support\Facades\Auth;
use Validator;


class SupportController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        if (Auth::user()->role == 'admin'){
            $regions = Region::all();
            $morrisData = [];
            $k = 0;
            foreach ($regions as $region){
                array_push($morrisData, "{x: '".$region->name."', y: ".$k++."}");
            }
            return view('home', compact('regions', 'morrisData'));
        }
        else {
            return redirect('/request');
        }

    }

}
